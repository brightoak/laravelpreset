require('./bootstrap');

window.Vue = require('vue');

/**
 * Add your custom VueJS components here
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});