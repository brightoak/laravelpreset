<?php

namespace Brightoak\LaravelPreset;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Console\PresetCommand;

class BrightoakPresetServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        PresetCommand::macro('brightoak', function($command) {
            $command->info('Installing Bright Oak Laravel Preset ...');
            Preset::install();
            $command->info('Bright Oak scaffolding installed successfully!');
            $command->info('Important! Run: "npm install && npm run dev" to compile your fresh scaffolding.');
            $command->info('You will then need to run node_modules/.bin/tailwind init to initialize your Tailwind config');
            $command->info('Run: "npm run dev" to finalize our mix manifest file');
        });
    }
}
