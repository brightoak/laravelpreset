# 🚀 Bright Oaks Laravel Frontend Preset

A Laravel frontend preset that scaffolds out new applications just the way we like 'em 👌🏻

What it includes:

- [Tailwind CSS](https://tailwindcss.com)
- [Vue.js](https://vuejs.org/)
- Removes Bootstrap and jQuery
- Adds compiled assets to `.gitignore`
- Adds a simple Tailwind-tuned default layout template
- Replaces the `welcome.blade.php` template with one that extends the main layout

## Installation

To get started, add it as a repository to your `composer.json` file:

```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/brightoak/laravelpreset"
    }
]
```

Next, run this command to add the preset to your project:

```
composer require brightoak/laravelpreset
```

Apply the scaffolding by running:

```
php artisan preset brightoak
```

Next, we need to run NPM

```
npm install && npm run dev
```

Finally, initialize Tailwind:

```
./node_modules/.bin/tailwind init
```

*Note*: If you get an error: *Mix manifest file is missing* You may want to run NPM watch for further changes:
```
npm run watch
```

For more information on using [Tailwind CSS]https://tailwindcss.com/docs/installation
